import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_architecture/core/enum/viewstate.dart';
import 'package:provider_architecture/core/models/post.dart';
import 'package:provider_architecture/core/models/user.dart';
import 'package:provider_architecture/core/viewmodels/home_model.dart';
import 'package:provider_architecture/ui/shared/app_colors.dart';
import 'package:provider_architecture/ui/shared/text_styles.dart';
import 'package:provider_architecture/ui/shared/ui_helpers.dart';
import 'package:provider_architecture/ui/widgets/postlist_item.dart';

import 'base_view.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    try{
      print('check ${Provider.of<User>(context).toJson()}');
    }catch(e){
      throw new Exception(e);
    }
    return BaseView<HomeModel>(
      onViewModelReady: (viewModel) => {
          viewModel.getPosts(Provider.of<User>(context,listen: true).id)
      },
      builder: (context, viewModel, child) => Scaffold(
        backgroundColor: backgroundColor,
        body: viewModel.state == ViewState.Busy
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  UIHelper.verticalSpaceLarge(),
                  Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Text(
                        'Welcome ${Provider.of<User>(context).id}',
                        style: headerStyle,
                      )),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: Text(
                      'Here are all your post',
                      style: subHeaderStyle,
                    ),
                  ),
                  UIHelper.verticalSpaceSmall(),
                  Expanded(
                    child: getPostUi(viewModel.posts),
                  )
                ],
              ),
      ),
    );
  }
}

Widget getPostUi(List<Post> posts) => ListView.builder(
    itemCount: posts.length,
    itemBuilder: (context, index) => PostListItem(
          post: posts[index],
          onTap: () {
            Navigator.pushNamed(context, 'post', arguments: posts[index]);
          },
        ));
