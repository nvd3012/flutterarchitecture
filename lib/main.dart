import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_architecture/core/services/AuthenticationService.dart';
import 'package:provider_architecture/locator.dart';
import 'package:provider_architecture/ui/route.dart';

import 'core/models/user.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>(
      create: (BuildContext context) => locator<AuthenticationService>().userController.stream,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(),
        initialRoute: "login",
        onGenerateRoute: Router.generateRoute,
      ),
    );
  }
}
