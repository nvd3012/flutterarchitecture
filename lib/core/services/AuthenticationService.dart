import 'dart:async';

import 'package:provider_architecture/core/models/user.dart';
import 'package:provider_architecture/core/services/api.dart';
import 'package:provider_architecture/locator.dart';

class AuthenticationService {
  Api _api = locator<Api>();
  StreamController<User> userController = StreamController<User>();

  Future<bool> login(int userId) async {
    var fetchedUser = await _api.getUserProfile(userId);
    print("user ${fetchedUser.toJson()}");
    if(fetchedUser!=null){
      userController.add(fetchedUser);
    }
    return fetchedUser!=null;
  }
}
