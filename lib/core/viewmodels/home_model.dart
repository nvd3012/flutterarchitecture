import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:provider_architecture/core/enum/viewstate.dart';
import 'package:provider_architecture/core/models/post.dart';
import 'package:provider_architecture/core/models/user.dart';
import 'package:provider_architecture/core/services/api.dart';
import 'package:provider_architecture/core/viewmodels/base_viewmodel.dart';
import 'package:provider_architecture/locator.dart';

class HomeModel extends BaseViewModel {
  Api _api = locator<Api>();

  List<Post> posts;

  Future getPosts(int userId) async {
    print("id getpost1 $userId");
    setState(ViewState.Busy);
    posts = await _api.getPostsForUser(userId);
    print("id getpost2 $userId");
    setState(ViewState.Idle);
  }
}
