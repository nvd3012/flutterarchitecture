import 'package:get_it/get_it.dart';
import 'package:provider_architecture/core/services/AuthenticationService.dart';
import 'package:provider_architecture/core/services/api.dart';
import 'package:provider_architecture/core/viewmodels/home_model.dart';
import 'package:provider_architecture/core/viewmodels/login_model.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(()=>Api());
  locator.registerLazySingleton(()=>AuthenticationService());

  locator.registerFactory(() => LoginModel());
  locator.registerFactory(() => HomeModel());
}